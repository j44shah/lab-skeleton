const db = process.argv[3];
const PROTO_PATH = "./app.proto";
const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");
const { uuid } = require("uuidv4");

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
});

const app = grpc.loadPackageDefinition(packageDefinition).App;
init();

async function randomProduct(call, callback) {
  const randProd = await queryRandomProduct();
  callback(null, randProd);
}

async function allProducts(call, callback) {
  const products = await queryAllProducts();
  callback(null, { products });
}

async function product(call, callback) {
  const { product_id } = call.request;
  const product = await queryProductById(product_id);
  callback(null, product);
}

async function categories(call, callback) {
  const categories = await queryAllCategories();
  callback(null, { categories });
}

async function allOrders(call, callback) {
  const orders = await queryAllOrders();
  callback(null, { orders });
}

async function ordersByUser(call, callback) {
  const { id } = call.request;
  const orders = await queryOrdersByUser(id);
  callback(null, { orders });
}

async function order(call, callback) {
  const { order_id } = call.request;
  const order = await queryOrderById(order_id);
  callback(null, order);
}

async function user(call, callback) {
  const { id } = call.request;
  const user = await queryUserById(id);
  callback(null, user);
}

async function users(call, callback) {
  const users = await queryAllUsers();
  callback(null, { users });
}

/// TODO: Implement postOrder here

async function accountDetails(call, callback) {
  const { id } = call.request;
  delete call.request.id;
  const user = await updateUser(id, call.request);
  callback(null, user);
}

/**
 * Starts an RPC server that receives requests for the Greeter service at the
 * sample server port
 */

const server = new grpc.Server();
server.addService(app.service, {
  getRandomProduct: randomProduct,
  getAllProducts: allProducts,
  getProduct: product,
  getAllCategories: categories,
  getAllOrders: allOrders,
  getAllUserOrders: ordersByUser,
  getOrder: order,
  getUser: user,
  getAllUsers: users,
  ///TODO: add postOrder:
  patchAccountDetails: accountDetails,
});

server.bindAsync(
  "0.0.0.0:3001",
  grpc.ServerCredentials.createInsecure(),
  () => {
    console.log("App ready and listening on port 3001");
    server.start();
  },
);
