const db = process.argv[3];

const {
  init,
  queryRandomProduct,
  queryUserById,
  queryAllProducts,
  queryProductById,
  queryAllCategories,
  queryAllOrders,
  queryOrdersByUser,
  queryOrderById,
  queryAllUsers,
  insertOrder,
  updateUser,
} = require(db === "dynamo"
  ? "./dynamo_db.js"
  : db === "sql"
  ? "./mysql_db.js"
  : "");

const getRandomProduct = async (req, res) => {
  const randProd = await queryRandomProduct();
  res.send(randProd);
};

const getProduct = async (req, res) => {
  const { productId } = req.params;
  const product = await queryProductById(productId);
  res.send(product);
};

const getProducts = async (req, res) => {
  const { category } = req.query;
  const products = await queryAllProducts(category);
  res.send(products);
};

const getCategories = async (req, res) => {
  const categories = await queryAllCategories();
  res.send(categories);
};

const getAllOrders = async (req, res) => {
  const orders = await queryAllOrders();
  res.send(orders);
};

const getOrdersByUser = async (req, res) => {
  const { userId } = req.query;
  const orders = await queryOrdersByUser(userId);
  res.send(orders);
};

const getOrder = async (req, res) => {
  const { orderId } = req.params;
  const order = await queryOrderById(orderId);
  res.send(order);
};

const getUser = async (req, res) => {
  const { userId } = req.params;
  const user = await queryUserById(userId);
  res.send(user);
};

const getUsers = async (req, res) => {
  const users = await queryAllUsers();
  res.send(users);
};

///TODO: Implement controller for POST /orders here

const patchUser = async (req, res) => {
  const updates = req.body;
  const { userId } = req.params;
  const response = await updateUser(userId, updates);
  res.send(response);
};

module.exports = {
  init,
  getProduct,
  getRandomProduct,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getProducts,
  getUser,
  getUsers,
  ///TODO: Export controller for POST /orders,
  patchUser,
};
