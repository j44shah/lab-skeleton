const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const app = express();
const port = 3000;

app.use(cors());
app.use(morgan("tiny"));
app.use(bodyParser.json());

const {
  init,
  getProduct,
  getRandomProduct,
  getProducts,
  getCategories,
  getAllOrders,
  getOrdersByUser,
  getOrder,
  getUser,
  getUsers,
  ///TODO: Write controller function for POST /orders,
  patchUser,
} = require("./controller.js");

init();

app.get("/", (req, res) => res.send("Hello, World!"));
app.get("/product/:productId", getProduct); // Gets a product by product id
app.get("/randomproduct", getRandomProduct); // I'm feeling lucky type
app.get("/products", getProducts); // Gets all products, or by category
app.get("/categories", getCategories); // Gets all categories
app.get("/allorders", getAllOrders);
app.get("/orders", getOrdersByUser); // Gets all of a single user's orders
app.get("/order/:orderId", getOrder); // Gets more details on a specific order by id
app.get("/user/:userId", getUser); // Gets details on a specific user by username
app.get("/users", getUsers);
///TODO: Implement POST /orders for order creation
app.patch("/user/:userId", patchUser);

app.listen(port, () => {
  console.log(`App ready and listening on port ${port}`);
});
